% mmda2.m
%
% T. Sim, S. Zhang, Jianran Li and Yan Chen, "Simultaneous and orthogonal decomposition of data using Multimodal Discriminant Analysis,"
% 2009 IEEE 12th International Conference on Computer Vision, Kyoto, 2009, pp. 452-459.
%
% Matlab Code Implementation:-
% Org: FOE Multimedia University&Motorola Penang
% Date: 3 Apr 2017
% Author: Lee Zun Ci
%
function [P, Pr, V1, V3, Dt, m, wX, w_mC, nC] = mmda2(X, N, dim, label, nMode, num_class)

    %2. Calculate Global mean
    %m = mean(X,2);     %calculate original global mean
    m = zeros([dim 1]); %zero global mean
    
    %3. Calculate Whitening Matrix P, Reverse Whitening Matrix Pr
    Ht = X - repmat(m,1,N);
    r_t = N - 1;
    %r_t = rank(Ht.' * Ht);
    
    %fast way to calculate using SVD instead of EIG
    %note: return values in decending order for both sqrt D&U, to recover D, D=SD^2
    %ref: http://www.cs.cmu.edu/~pmuthuku/mlsp_page/assignments/assignment2_hints.html
    [U,SD,~] = svd(Ht,0);
    D = diag(SD).^2;
    
    %resize according to rank
    D = diag(D(1:r_t,:));
    U = U(:,1:r_t);
    
    %P,Pr
    %ref: http://stats.stackexchange.com/questions/117427/what-is-the-difference-between-zca-whitening-and-pca-whitening
    P = U*sqrt(inv(D)); %PCA Whitening Matrix
    Pr = U*sqrt(D);     %Reverse PCA Whitening Matrix
    
    %4. Calculate Whitened data X
    wX = P.' * X;

    %5. Now repeat WFLD on each modes
    for i=1:nMode
        %msg = ['- on Mode ' num2str(i)];
        %disp(msg);
        [V1{i}, V3{i}, Dt{i}, w_mC{i}, nC{i}] = wfld(X, wX, P, label(:,i), num_class(i), N);
    end
end