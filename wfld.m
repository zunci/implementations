% wfld.m
%
% S. Zhang, T. Sim and Mei-Chen Yeh, "Identity and Variation Spaces: Revisiting the Fisher Linear Discriminant,"
% 2009 IEEE 12th International Conference on Computer Vision Workshops, ICCV Workshops, Kyoto, 2009, pp. 123-130.
%
% Matlab Code Implementation:-
% Org: FOE Multimedia University&Motorola Penang
% Date: 3 Apr 2017
% Author: Lee Zun Ci
%
function [V1i, V3i, Di, w_mCi, nCi] = wfld(X, wX, P, label, num_class, Nx)

    %1. Calculate:-
    %   Whitened Between Scatter Matrix, w_Sb
    %   Whitened Within Scatter Matrix, w_Sw
    
    w_mCi = [];
    nCi = [];
    
    for i=1:num_class
        idx_class =(label(:)==i);    %get index of class based on label
        Xi = X(:,idx_class);         %X of each class
        wXi = wX(:,idx_class);       %Whitened X of each class
        mC{i} = mean(Xi,2);          %Class mean
        w_mC{i} = P.' * mC{i};       %Whitened class mean
        w_mCi = [w_mCi w_mC{i}];
        [h, ~, ~] = size([w_mC{i}]);
        nC{i} = size(Xi,2);          %Number of labels in each class
        nCi = [nCi nC{i}];
        %init first index
        if i==1
        tmpwSb{1} = zeros(h,h);
        tmpwSw{1} = zeros(h,h);
        end
        %cummulative
        tmpwSb{i+1} = tmpwSb{i} + (nC{i} * (([w_mC{i}]) * ([w_mC{i}]).'));
        tmpwSw{i+1} = tmpwSw{i} + ((wXi - repmat(w_mC{i},1,nC{i})) * ((wXi - repmat(w_mC{i},1,nC{i})).'));
    end

    %last index is the sum
    w_Sb = [tmpwSb{num_class + 1}];
    w_Sw = [tmpwSw{num_class + 1}];

    %2. Maximise J_F
    [Vi, Di] = eig(w_Sb, w_Sw);                     %AV = BVD, usage: [V,D] = eig(A,B)
    [Di, idx] = sort((diag(Di)),'descend');         %Sort Eigenvalues decendingly
    Vi = Vi(:,idx);                                 %Sort Eigenvector accordingly
    id_count = num_class - 1;%sum(Di(:) == Inf);                   %Identity Space: C-1    %num_class - 1; 
    %var_count = Nx - num_class;                    %Variation Space: N-C
    V1i = Vi(:,1:id_count);                         %Decompose to Identity Spaces, V1
    V3i = Vi(:,id_count+1:end);                     %Decompose to Variation Spaces, V3
end