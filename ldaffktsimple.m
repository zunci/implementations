% ldafktsimple.m
%
% Sheng Zhang and T. Sim, "When Fisher meets Fukunaga-Koontz: A New Look at Linear Discriminants,"
% 2006 IEEE Computer Society Conference on Computer Vision and Pattern Recognition (CVPR'06), 2006, pp. 323-329.
%
% Matlab code:-
% (1) LDA/FKT Algorithm in Figure2: Apply QR decomposition to compute LDA/FKT
% (2) Sample Data: '4.1 A Toy Problem'
%
clear all;

% Toy Problem Data
cov_mat = diag([1,1,0]); % covariance matrix
mu1 = [0; 0; 0];         % mean class 1
mu2 = [0; 1; 2];         % mean class 2
mu3 = [0; 1; 0];         % mean class 3
num_points = 10;         % number of points
Nc = [num_points;num_points;num_points]; % number of points for each class
C = 3;

% Generate Gaussian
r1 = mvnrnd(mu1, cov_mat, num_points); %class1
r2 = mvnrnd(mu2, cov_mat, num_points); %class2
r3 = mvnrnd(mu3, cov_mat, num_points); %class3
figure(1), subplot(1,3,1), scatter3(r1(:,1),r1(:,2),r1(:,3),'kx'), title('Original 3D');
hold on
scatter3(r2(:,1),r2(:,2),r2(:,3),'r^');
hold on
scatter3(r3(:,1),r3(:,2),r3(:,3),'bo');
hold off

%make D x N col vector
a1 = r1';
a2 = r2';
a3 = r3';

%global mean = 0 (assumption for generality)
%else, m = mean([a1,a2,a3],2);
m = [0;0;0];

% Step1: Calculate H_b and H_t
% H_b: D x C
% H_w: D x N
% H_t: D x N

Hb = [sqrt(Nc(1))*(mu1 - m), sqrt(Nc(2))*(mu2 - m), sqrt(Nc(3))*(mu3 - m)]; %H_b: D x C: 3 x 3
Ht = [a1 - repmat(m,1,Nc(1)),a2 - repmat(m,1,Nc(2)),a3 - repmat(m,1,Nc(3))]; %H_t: D x N: 3 x 30: (10 points per class)

% Step2: H_t = QR
% Q: D x r_t: 3 x 3
% R: r_t x N: 3 x 30
[Q,R] = qr(Ht,0);

% Step3: Calculate whitened total scatter matrix,
% w_S_t = Q'*S_t*Q = Q'*H_t*H_t'*Q = R * R'
% with H_t = Q*R, then R = inv(Q)*H_t = Q'*H_t (note that for positive semi-definite matrix,
% its inverse and its tranpose are equal)
w_St = R * R';

% Step4: Let Z = Q'*H_b
Z = Q' * Hb;

% Step5: Calculate whitened between scatter matrix, w_S_b
% w_S_b = Z*Z' = Q'*H_b*H_b'*Q = Q'*S_b*Q
w_Sb = Z * Z';

% Step6: Find eigenvalues,(lamda) and eigenvectors,(v) of inv(w_St)*(w_Sb)
tmp = w_St \ w_Sb;
[V, D] = eig(tmp);

% Step7: sort eigenvalues decreasing&&>=0, eigenvectors accordingly
k = C - 1;
[D,idx] = sort((diag(D)),'descend');
figure(1), subplot(1,3,2), plot((D(:,1)),'b-o'), title('Eigenvalues');
hold on
plot(1 - D(:,1),'r--^');
hold off

D = diag(D(1:k,:));
V = V(:,idx);
Vk = V(:,1:k); %Vk

% Step8: final projection matrix phi_F = Q*V, select V according to col
% correspond to subspace1,2,3
phi_F = Q * Vk;

% Step 9: project to subspace1,2
% yi = phi_F' * ai
y1 = phi_F' * a1;
y2 = phi_F' * a2;
y3 = phi_F' * a3;

% Scatter plot of LDA/FKT projection in 2D Space
figure(1), subplot(1,3,3),scatter(y1(1,:),y1(2,:), 'kx'), title('LDA/FKT 2D');
hold on
scatter(y2(1,:),y2(2,:),'r^');
hold on
scatter(y3(1,:),y3(2,:),'bo');
hold off